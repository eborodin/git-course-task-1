# Удаление ненужных коммитов
Придумать как можно удалить все коммиты из истории кроме трех последних


# Expected result:
- Публичный репозиторий
- Склоненный с репозитория https://github.com/MaksymSemenykhin/git-course-task/tree/task%231
- В истории комитов только три коммита(последние в оригинальной репе)
- В README файле описано как именно был произведен откат(Схема, описание или скрипт)
- Правки README файла могут быть 4м коммитом

# Task#1 Actions:
git clone https://gitlab.com/eborodin/git-course-task-1.git
git co --orphan temp
git reset --hard 
git cherry-pick 929f5e1
git add .
git cherry-pick --continue
git cherry-pick d4d983e
git cherry-pick 6cb2c11
git br -D master
git br -m task#1
git push -f --set-upstream origin task#1

# add actions to README.md

git add -u
git commit -m "task#1 actions list"
git push

